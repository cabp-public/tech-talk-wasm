const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');

const htmlPlugin = new HtmlWebPackPlugin({
  template: './src/index.html',
  filename: './index.html'
});

const copyPlugin = new CopyWebpackPlugin([
  {
    from: './src/assets/**/*.+(jpg|png)',
    to: './assets/[name].[ext]',
    toType: 'template'
  },
  {
    from: './src/modules/**/*.+(wasm)',
    to: './modules/[name].[ext]'
  }
]);

const writePlugin = new WriteFilePlugin({
  test: /\.(jpg|ico|mp3|wasm)$/,
  useHashIndex: false
});

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(jpg|png)$/,
        use: {
          loader: 'url-loader'
        }
      },
      {
        test: /\.(ttf|eot|woff|woff2|wasm)$/,
        use: {
          loader: 'file-loader'
        }
      }
    ]
  },
  devServer: {
    historyApiFallback: true,
  },
  plugins: [htmlPlugin, copyPlugin, writePlugin]
};