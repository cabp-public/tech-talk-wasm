import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Dashboard from './components/Dashboard';
import Landing from './components/Landing';
import NotFound from './components/NotFound';

const ProjectApp = () => (
  <Router>
    <div>
      <Switch>
        <Route path="/" exact component={Dashboard} />

        <Route path="/dashboard" exact component={Dashboard} />
        <Route path="/project/create" exact component={Landing} />

        <Route component={NotFound} />
      </Switch>
    </div>
  </Router>
);

export default ProjectApp;