import React, { Component } from 'react';
import * as M from 'materialize-css';

import PROJECT_APP from './../';
import Preloader from './Preloader';
import Navigation from './Navigation';
import Pager from './Pager';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.module = null;

    this.state = {
      totalPages: 0,
      totalItemsPerPage: 0,
      loading: false
    };

    this.onModulesLoaded = this.onModulesLoaded.bind(this);
    this.renderRows = this.renderRows.bind(this);
  }

  componentDidMount() {
    this.setState({ loading: true });
    PROJECT_APP.ModuleLoader.load(this.constructor.name, this.onModulesLoaded);
  }

  componentWillReceiveProps(nextProps) {
  }

  onModulesLoaded(module) {
    this.module = module;

    const pingVal = 2;
    const pongVal = this.module._ping(pingVal);
    const totalPages = this.module._getTotalPages();
    const itemsPerPage = this.module._getItemsPerPage();

    console.log(this.constructor.name + ' just got its own module...!');
    console.log('Ping: ' + pingVal);
    console.log('Pong: ' + pongVal);

    this.setState({
      totalPages,
      itemsPerPage,
      loading: false
    });
  }

  renderRows() {
    const rows = [];
    const { itemsPerPage } = this.state;

    for ( let i = 0; i < itemsPerPage; i++ ) {
      rows.push(
        <tr key={i}>
          <td>Lorem Ipsum</td>
          <td>45</td>
          <td>November 20, 2018</td>
        </tr>
      );
    }

    return rows;
  }

  render() {
    const { totalPages, loading } = this.state;
    const { name } = this.constructor;

    return (
      <div>
        <Navigation />

        <table>
          <thead>
          <tr className="black">
            <th>Name</th>
            <th>Tasks</th>
            <th>Last Update</th>
          </tr>
          </thead>

          <tbody>
          { this.renderRows() }
          </tbody>
        </table>

        <Pager totalPages={totalPages} />
        <Preloader loading={loading} label={name} />
      </div>
    )
  }
}

export default Dashboard;
