import React, { Component } from 'react';
import * as M from 'materialize-css';

class Preloader extends Component {
  constructor(props) {
    super(props);

    this.instance = {};

    this.componentWillReceiveProps = this.componentWillReceiveProps.bind(this);
  }

  componentDidMount() {
    const { loading } = this.props;
    const element = this.refs['modal-preloader'];

    this.instance = M.Modal.init(element, {
      'dismissible': false,
      'opacity': 0.75,
      'startingTop': '20%',
      'endingTop': '25%'
    });

    loading && this.instance.open();
  }

  componentWillReceiveProps(nextProps) {
    const { loading } = nextProps;
    loading ? this.instance.open() : ( this.instance.isOpen && this.instance.close() );
  }

  render() {
    const { label } = this.props;

    return (
      <div>
        <div ref="modal-preloader" className="modal-preloader modal transparent z-depth-0">
          <div className="modal-content transparent center-align">
            <div className="preloader-wrapper big active">
              <div className="spinner-layer spinner-blue">
                <div className="circle-clipper left">
                  <div className="circle"/>
                </div>
                <div className="gap-patch">
                  <div className="circle"/>
                </div>
                <div className="circle-clipper right">
                  <div className="circle"/>
                </div>
              </div>
              <div className="spinner-layer spinner-red">
                <div className="circle-clipper left">
                  <div className="circle"/>
                </div>
                <div className="gap-patch">
                  <div className="circle"/>
                </div>
                <div className="circle-clipper right">
                  <div className="circle"/>
                </div>
              </div>
              <div className="spinner-layer spinner-yellow">
                <div className="circle-clipper left">
                  <div className="circle"/>
                </div>
                <div className="gap-patch">
                  <div className="circle"/>
                </div>
                <div className="circle-clipper right">
                  <div className="circle"/>
                </div>
              </div>
              <div className="spinner-layer spinner-green">
                <div className="circle-clipper left">
                  <div className="circle"/>
                </div>
                <div className="gap-patch">
                  <div className="circle"/>
                </div>
                <div className="circle-clipper right">
                  <div className="circle"/>
                </div>
              </div>
            </div>

            <label className="center-block">LOADING { label.toUpperCase() }</label>
          </div>
        </div>
      </div>
    );
  }
}

export default Preloader;
