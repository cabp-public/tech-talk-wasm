import React, { Component } from 'react';
import * as M from 'materialize-css';

import Navigation from './Navigation';

class NotFound extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    M.AutoInit();
  }

  componentWillReceiveProps(nextProps) {
  }

  render() {
    return (
      <div>
        <Navigation />
        404
      </div>
    )
  }
}

export default NotFound;
