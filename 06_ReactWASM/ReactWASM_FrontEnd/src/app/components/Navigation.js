import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Navigation extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const element = this.refs['dom-sidenav'];
    const instance = M.Sidenav.init(element);
  }

  componentWillReceiveProps(nextProps) {
  }

  render() {
    return (
      <div>
        <ul ref="dom-sidenav" id="slide-out" className="sidenav yatdl-component-navigation">
          <li>
            <div className="user-view">
              <div className="background">
                <img src="https://materializecss.com/images/office.jpg" />
              </div>
              <img className="circle" src="https://materializecss.com/images/yuna.jpg" />
                <span className="white-text name">John Doe</span>
                <span className="white-text email">john.doe@hotmail.com</span>
            </div>
          </li>

          <li>
            <Link to="/project/create" className="grey-text waves-effect">
              <i className="material-icons">add_comment</i>New Project
            </Link>
          </li>

          <li>
            <Link to="/project/create" className="grey-text waves-effect">
              <i className="material-icons">add_comment</i>New Task
            </Link>
          </li>

          <li><div className="divider"/></li>
          <li><a href="#" className="waves-effect"><i className="material-icons">power_settings_new</i>Logout</a></li>
        </ul>

        <a href="#" data-target="slide-out" className="sidenav-trigger">
          <i className="material-icons grey-text text-lighten-1">menu</i>
        </a>
      </div>
    )
  }
}

export default Navigation;
