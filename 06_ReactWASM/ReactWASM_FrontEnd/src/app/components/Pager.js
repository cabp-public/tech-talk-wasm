import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Pager extends Component {
  constructor(props) {
    super(props);

    this.renderLinks = this.renderLinks.bind(this);
  }

  componentDidMount() {
  }

  componentWillReceiveProps(nextProps) {
  }

  renderLinks() {
    const { totalPages } = this.props;
    const children = [];

    for ( let i = 0; i < totalPages; i++ ) {
      children.push(
        <div key={i} className="chip light-blue darken-4 grey-text text-lighten-1">
        { i + 1 }
        </div>
      );
    }

    return children;
  }

  render() {
    const { totalPages } = this.props;

    return (
      <div className="pager-bottom center-align">
        { this.renderLinks() }
      </div>
    )
  }
}

export default Pager;
