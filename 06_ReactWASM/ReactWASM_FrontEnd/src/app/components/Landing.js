import React, { Component } from 'react';
import * as M from 'materialize-css';

import Navigation from './Navigation';

class Landing extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    M.AutoInit();
  }

  componentWillReceiveProps(nextProps) {
  }

  render() {
    return (
      <div>
        <Navigation />
      </div>
    )
  }
}

export default Landing;
