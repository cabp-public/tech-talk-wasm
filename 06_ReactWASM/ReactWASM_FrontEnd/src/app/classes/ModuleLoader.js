import MasterClass from './MasterClass';
import * as appSettings from './../../config/appSettings';

let singleModule = null;
let eventListenerExternal = null;

class ModuleLoader extends MasterClass {
  constructor(main, options) {
    super(main, options);

    this.options.origin = document.location.origin;

    this.eventListener = this.eventListener.bind(this);
  }

  eventListener(event) {
    setTimeout(function () {
      eventListenerExternal(singleModule);
    }, 1000);
  }

  load(componentName, eventListener) {
    const eventPrefix = 'loadingModule_';
    const eventName = eventPrefix + componentName;
    const event = new CustomEvent(eventName);
    const settings = appSettings.components[componentName];
    const moduleName = componentName.toLowerCase();
    const { ownsModule } = settings;
    const { requiredModules } = settings;
    let modulesUrl, moduleUrl;

    eventListenerExternal = eventListener;
    document.addEventListener(eventName, this.eventListener);

    modulesUrl  = this.options.origin + '/modules/';
    moduleUrl = ownsModule ? (modulesUrl + moduleName + '.wasm') : '';

    // Own module
    const memory = new WebAssembly.Memory({ initial: 256, maximum: 256 });
    const importObj = {
      env: {
        abortStackOverflow: () => { throw new Error('overflow'); },
        table: new WebAssembly.Table({ initial: 0, maximum: 0, element: 'anyfunc' }),
        tableBase: 0,
        memory: memory,
        memoryBase: 1024,
        STACKTOP: 0,
        STACK_MAX: memory.buffer.byteLength,
      }
    };

    fetch(moduleUrl).then((response) => response.arrayBuffer())
      .then((bytes) => WebAssembly.instantiate(bytes, importObj))
      .then((wasm) => {
        const { exports } = wasm.instance;

        singleModule = exports;
        document.dispatchEvent(event);
      });

    // Required modules
  }
}

export default ModuleLoader;