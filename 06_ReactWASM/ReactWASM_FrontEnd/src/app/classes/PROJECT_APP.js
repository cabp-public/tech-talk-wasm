// Classes
import ModuleLoader from './ModuleLoader';

// Models
// import MemberModel from './models/MemberModel';
// import ItemModel from './models/ItemModel';

// Components
// import ErrorModal from './components/ErrorModal/ErrorModal';
// import Preloader from './components/Preloader/Preloader';
// import SignUp from './components/SignUp/SignUp';
// import Navigation from './components/Navigation/Navigation';
// import CreateItem from './components/CreateItem/CreateItem';
// import GenericList from './components/GenericList/GenericList';
// import ItemCounter from './components/ItemCounter/ItemCounter';

class PROJECT_APP {
  init(options) {
    const initEventReady = new CustomEvent(PROJECT_APP.initEvent);

    // Main Members
    this['modules'] = {};
    this['models'] = {};
    this['components'] = {
      // ErrorModal: new ErrorModal(this, options.components['ErrorModal'] || {})
    };

    // Init ErrorModal
    // this.components.ErrorModal.init();

    // Classes
    // console.log(ModuleLoader);
    const classesAvail = {
      ModuleLoader: new ModuleLoader(this, options.classes.ModuleLoader || {})
    };

    // Models
    const modelsAvail = {
      // MemberModel: new MemberModel(this, options.models.MemberModel || {}),
      // ItemModel: new ItemModel(this, options.models.ItemModel || {})
    };

    // Components
    const componentsAutoInit = {
      // Preloader: new Preloader(this, options.components.Preloader || {}),
      // SignUp: new SignUp(this, options.components.SignUp || {}),
      // Navigation: new Navigation(this, options.components.Navigation || {}),
      // CreateItem: new CreateItem(this, options.components.CreateItem || {}),
      // GenericList: new GenericList(this, options.components.GenericList || {}),
      // ItemCounter: new ItemCounter(this, options.components.ItemCounter || {})
    };

    Object.keys(classesAvail).forEach((key) => this[key] = classesAvail[key]);
    Object.keys(modelsAvail).forEach((key) => this['models'][key] = modelsAvail[key]);
    Object.keys(componentsAutoInit).forEach((key) => this['components'][key] = componentsAutoInit[key]);

    document.dispatchEvent(initEventReady);
  }
}

PROJECT_APP.initEvent = 'INIT_READY';

export default PROJECT_APP;