import React from 'react';
import { render } from 'react-dom';

import * as initOptions from './config/initOptions';
import PROJECT_APP from './app/';
import ProjectApp from './app/ProjectApp';
// import ProjectReactApp from './app/ProjectReactApp';
import './../node_modules/materialize-css/dist/css/materialize.min.css';
import './assets/css/custom.css';

const { constructor: { initEvent } } = PROJECT_APP;

document.addEventListener(initEvent, function (event) {
  render(<ProjectApp/>, document.getElementById('projectAppEl'));

  // const { StoreManager } = ReactMarketApp;
  // render(<Provider store={ StoreManager.reduxStore }><MarketApp/></Provider>, document.getElementById('index'));
});

PROJECT_APP.init(initOptions.initOptions);
