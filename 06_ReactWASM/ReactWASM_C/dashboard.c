#include <stdio.h>

int main() {
    return 0;
}

int ping(int value) {
    return value * 2;
}

int getTotalProjects() {
    return 50;
}

int getItemsPerPage() {
    return 5;
}

int getTotalPages() {
    return getTotalProjects() / 10;
}
