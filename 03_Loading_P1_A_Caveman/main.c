#include <stdio.h>
#include <math.h>
#include <emscripten/emscripten.h>

int main() {
    printf("Greetings from C 11!\n");
    return 0;
}

int int_sqrt(int value) {
    return sqrt(value);
}

#ifdef __cplusplus
extern "C" {
#endif

void EMSCRIPTEN_KEEPALIVE outputString(char * value) {
    printf("%s \n", value);
}

int EMSCRIPTEN_KEEPALIVE getSqrt(int x) {
    return int_sqrt(x);
}

#ifdef __cplusplus
}
#endif