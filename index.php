<?php
$samples = [
    [
        'title' => '01 - Hello World',
        'children' => [
            ['title' => 'Hello C11', 'path' => '01_HelloC11/dist/'],
            ['title' => 'Hello C++ 17', 'path' => '02_HelloPlus17/dist/'],
        ]
    ],
    [
        'title' => '02 - Loading Part I',
        'children' => [
            ['title' => 'The Caveman Way', 'path' => '03_Loading_P1_A_Caveman/dist/caveman/'],
            ['title' => 'WASM Only', 'path' => '03_Loading_P1_B_WASM/dist/'],
        ]
    ],
    [
        'title' => '03 - Loading Part II',
        'children' => [
            ['title' => 'Several EMSCRIPTEN', 'path' => '04_Loading_P2_A_EMSCRIPTEN/dist/'],
            ['title' => 'Several WASM Only', 'path' => '04_Loading_P2_B_WASM/dist/'],
        ]
    ],
];
?>

<!doctype html>
<html lang="en-us">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

  <title>WASM - Code Samples</title>
</head>

<body>

<div class="container">
  <div class="row">
    <?php foreach ( $samples as $sample ) { ?>
      <div class="col s12 m6">
        <ul class="collection with-header">
        <?php if ( isset($sample['children']) ) { ?>
          <li class="collection-header"><h5><?=$sample['title']?></h5></li>
          <?php foreach ( $sample['children'] as $item ) { ?>
            <a href="/<?=$item['path']?>" target="_blank" class="collection-item"><?=$item['title']?></a>
          <?php } ?>
        <?php } ?>
        </ul>
      </div>
    <?php } ?>
  </div>
</div>

</body>

</html>


